This forms a simple distribution system for IDEA plugins.

Upload the plugin zip file via the Bitbucket Downloads section

Then edit updatePlugins.xml to point to the latest version of the plugin


Consumers can then add `https://bitbucket.org/atlassian/idea-plugin-repository/raw/master/updatePlugins.xml` to their IDEA plugin custom repository and start to download your plugin